app.config(function($stateProvider, $urlRouterProvider, $httpProvider) {
    $stateProvider.state('app', {
            url: '/app',
            abstract: true,
            templateUrl: 'templates/partials/menu.html',
            controller: 'AppController'
        }).state('login', {
            cache: false,
            url: '/login',
            templateUrl: 'templates/login.html',
            controller: 'LoginController'
        }).state('register', {
            cache: false,
            url: '/register',
            templateUrl: 'templates/register.html',
            controller: 'RegisterController'
        }).state('app.index', {
             cache: false,
            url: '/index',
            views: {
                menuContent :{
                    templateUrl: 'templates/index.html',
                    controller : 'IndexController'
                }
            }
        }).state('app.medicine', {
            cache: false,
            url: '/medicine',
            views: {
                menuContent :{
                    templateUrl: 'templates/medicine.html',
                    controller : 'MedicineController'
                }
            }
        }).state('app.mymedicine', {
            cache: false,
            url: '/mymedicine',
            views: {
                menuContent :{
                    templateUrl: 'templates/mymedicine.html',
                    controller : 'MyMedicineController'
                }
            }
        }).state('app.consuption', {
            cache: false,
            url: '/consuption',
            views: {
                menuContent :{
                    templateUrl: 'templates/consuption.html',
                    controller : 'ConsuptionController'
                }
            }
        }).state('app.ranges', {
            cache: false,
            url: '/ranges',
            views: {
                menuContent :{
                    templateUrl: 'templates/ranges.html',
                    controller : 'RangeController'
                }
            }
        });
        

    $urlRouterProvider.otherwise('/login');

    $httpProvider.defaults.headers.common = {};
    $httpProvider.defaults.headers.post = {};
    $httpProvider.defaults.headers.put = {};
    $httpProvider.defaults.headers.patch = {};
});