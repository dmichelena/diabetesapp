app.controller('MedicineController', function($scope, $rootScope, userService, typeAheadService, $location, $ionicLoading, $ionicPopup) {
  
    $scope.itemSelected = undefined;
    $scope.items = [];
    $scope.itemList = [];
    $scope.search = '';
    $scope.$watch('search', function(newValue, oldValue) {
        if($scope.search == ''){
            return $scope.cancel();
        }
        typeAheadService.getMedicaments($scope.search).then(function (resp){
            $scope.items = $scope.filterSelected(resp.data);
        });
    });
    $scope.cancel = function () {
        $scope.items = [];
        $scope.search = '';
    };
    $scope.onItemSelected = function() {
        $scope.itemList.push($scope.itemSelected);
        $scope.cancel();
    };
    $scope.checkItemExist = function(item) {
        var count =  $scope.itemList.length;
        for(var i = 0; i < count; i++){
            if($scope.itemList[i].id == item.id){
                return true;
            }
        }
        return false;
    };
    $scope.filterSelected = function(items) {
        var count =  items.length;
        var listResponse = [];
        for(var i = 0; i < count; i++){
            if(!$scope.checkItemExist(items[i])){
                listResponse.push(items[i]);
            }
        }
        return listResponse;
    };
    $scope.delete = function(index){
        $scope.itemList.splice(index, 1);
    };
    $scope.save = function(){
        $ionicLoading.show({noBackdrop : false});
        userService.saveMedicines($scope.itemList) 
            .success(function(data, status, headers, config) {
                $ionicLoading.hide();
                 $ionicPopup.alert({
                    title: 'Aviso',
                    template: 'Se guardó con éxito su información.'
                });
                $scope.cancel();
                $location.path('/app/index');
            })
             .error(function(data, status, headers, config) {
                $rootScope.handleErrorForm($ionicLoading, $ionicPopup, status, data);
            });
    };
    $scope.cancelMedicine = function(){
        $scope.itemList = [];
        $scope.cancel();
        $location.path('/app/index');
    };
});