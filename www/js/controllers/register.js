app.controller('RegisterController', function($scope, $rootScope, $location, publicService, userService, $ionicLoading, $ionicPopup) {
    $scope.registerData = {};
    $scope.formIsValid = function() {
        return true;
    };
    $scope.register = function() {
        if(!$scope.formIsValid()){
            return;
        }
        $ionicLoading.show({noBackdrop : false});
        publicService.register($scope.registerData)
            .success(function(data, status, headers, config) {
                $ionicLoading.hide();
                $scope.registerData = {};
                userService.setUser(data);
                $location.path('/app/index');
            })
             .error(function(data, status, headers, config) {
                $rootScope.handleErrorForm($ionicLoading, $ionicPopup, status, data);
            });
    };
})