controllers.service('userService', function($interval, restService, $rootScope){
    var userObject = {};
    var sus = true;

    return {
        setUser: function (user){
            userObject = user;
            if (sus) {
				//FCMPlugin.subscribeToTopic('user' + user.id);
				sus = false;
            }
            $rootScope.accessToken = user.accessToken;
        },
        getUser: function (user){
            return userObject;
        },
        refresh : function () {
           return restService.post('user/refresh', {});
        },
        getName: function (){
            return userObject.firstName + ' ' +userObject.lastName;
        },
        isPatient: function (){
            return userObject.isPatient;
        },
        getGender: function (){
            return userObject.gender;
        },
        registerGlucoseIndex: function (data){
            return restService.post('user/register-glucose-index',data);
        },    
        getRanges: function (){
            return restService.get('range/get-all');
        },  
        registerRange: function (data){
            return restService.post('range/save',data);
        }, 
        deleteRange: function (data){
            return restService.post('range/delete',data);
        }, 
        saveMedicines: function (data){
            return restService.post('medicine/save',data);
        }, 
        saveFood: function (data){
            return restService.post('food/save',data);
        },   
        getMedicine: function (){
            return restService.get('medicine/get');
        },  
        getFood: function (){
            return restService.get('food/get');
        },
		getReport: function (){
			return restService.get('user/get-report');
		},
		sendAlert: function (){
			return restService.post('user/alert');
		},
		getConsuptionData: function (){
            return restService.get('food/get-data');
        },   
        activeMedicine: function (data){
            return restService.post('medicine/active', data);
        },        
        addGlucoseIndex: function (data){

        },
        logout: function (){
            return restService.post('user/logout');
        },
        deleteUserData: function (){
			//FCMPlugin.unsubscribeFromTopic('user' + userObject.id);
			sus = true;
            userObject = {};
            $rootScope.accessToken = undefined;
            $rootScope.endRefreshUser();
        }
    };
});