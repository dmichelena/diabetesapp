// Ionic Starter App
window.deviceControl = {
    callback: null,
	// Application Constructor
	initialize: function() {
		document.addEventListener('deviceready', this.call.bind(this), false);
	},
	call: function() {
		window.deviceControl.callback();
	},
	// deviceready Event Handler
	onDeviceReady: function(callback) {
		window.deviceControl.callback = callback;
	}
};

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
var app = angular.module('app', ['ionic', 'app.controllers', 'base64', 'chart.js'])
    .run(function ($ionicPlatform, $rootScope, $location, userService, $interval, $ionicPopup, pushService) {
		window.deviceControl.onDeviceReady(function () {
            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)
            if (window.cordova && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
                cordova.plugins.Keyboard.disableScroll(true);

            }
            if (window.StatusBar) {
                // org.apache.cordova.statusbar required
                StatusBar.styleDefault();
            }
            $rootScope.initData();
            $rootScope.allreadyInitData = true;
            $rootScope.decideRoute();
            $rootScope.registerDeviceId();
//			FCMPlugin.onNotification(function(data){
//				if(data.wasTapped){
//					// alert(JSON.stringify(data));
//				}else{
//					$ionicPopup.alert({
//						title: data.title,
//						template: data.body
//					});
//				}
//			});
        });
		window.deviceControl.initialize();

        $rootScope.typeOf = function (value) {
            return typeof value;
        };
        $rootScope.handleErrorForm = function ($ionicLoading, $ionicPopup, status, data){
            $ionicLoading.hide();
            if(status == 401)
            {
                return $rootScope.forceLogout();
            }
            if(status == 422 && data)
            {
                return $rootScope.showFormError($ionicPopup, data);
            }
            var title = (status >= 500 || !data || !data.message) ? 'Ups!' : 'Error';
            var template = (status >= 500 || !data || !data.message) ? 'Estamos presentando problemas' : data.message;
            $ionicPopup.alert({
                title: title,
                template: template
            });
        };
        $rootScope.showFormError = function ($ionicPopup, data) {
            var errors = '';
            for (var i in data) {
                errors += data[i].message + '</br>';
            }
            $ionicPopup.alert({
                title: 'Por favor revise los siguientes errores',
                template: errors
            });
        };

        $rootScope.allreadyInitData = false;
        $rootScope.$watch('accessToken', function(newValue, oldValue) {
            if(!newValue){
                $location.path('/login');
            }
            if($rootScope.allreadyInitData){
                $rootScope.refreshData();
            }
        });

        $rootScope.$watch('deviceId', function(newValue, oldValue) {
            if(newValue){
                 $rootScope.refreshData();
            }
        });

        /******************* DB ******************/
        $rootScope.getDb = function () {
            return window.localStorage;
        };

	    $rootScope.initData = function () {
            var db = $rootScope.getDb();

            var user= db.getItem('user');
            if(user && user != "undefined") {
                userService.setUser(JSON.parse(user));
            }

            var deviceId= db.getItem('deviceId');
            if(deviceId && deviceId != "undefined") {
                $rootScope.deviceId = deviceId;
            }
        };
     

        $rootScope.refreshData = function () {
            var db = $rootScope.getDb();

            if($rootScope.accessToken){
                db.setItem('user', JSON.stringify(userService.getUser()));
            } else {
                db.removeItem('user');
            }

            if($rootScope.deviceId){
                db.setItem('deviceId', $rootScope.deviceId);
            }
        };

        /******************* Prevent show other page with unlogged user ******************/
         $rootScope.$on('$locationChangeSuccess',function(event, newUrl, oldUrl) {
           var url = $location.url();
           if(url != '/login' && url != '/register' && !$rootScope.accessToken) {
                $location.path('/login');
           }
        });

        /******************* Check route init app ******************/
        $rootScope.decideRoute = function () {
            if($rootScope.accessToken){
                $rootScope.startRefreshUser();
                $location.path('/app/index');
            } else {
                $location.path('/login');
            }
        };


        /******************* REFRESH USER DATA ******************/
        var stop;
        $rootScope.startRefreshUser = function(login) {
            var callback = function() {
            userService.refresh()
                .success(function(data, status, headers, config) {
                    userService.setUser(data);
                })
                .error(function(data, status, headers, config) {
                        if(status == 401)
                        {
                            return $rootScope.forceLogout();
                        }
                });
          };
          stop = $interval(callback, 15000);
          if(!login){
            callback();
          }
        };

        $rootScope.endRefreshUser = function() {
            $interval.cancel(stop);
            stop = undefined;
        };

         /******************* FORCE LOGOUT ******************/
        $rootScope.forceLogoutCallback = undefined;
        $rootScope.forceLogout = function () {
            if($rootScope.forceLogoutCallback) {
                $rootScope.forceLogoutCallback();
                $rootScope.forceLogoutCallback = undefined;
            }
            userService.deleteUserData();
            $ionicPopup.alert({
                title: 'Alerta',
                template: 'Al parecer no te encuentas logueado, vuelve a iniciar sesión.'
            });
        };

        $rootScope.setCallback = function (callback) {
            $rootScope.forceLogoutCallback = callback;
        };

         /******************* TRY To GET DEVICE ID ******************/
        $rootScope.registerDeviceId = function () {
            if(!$rootScope.deviceId){
               try {
                    pushService.register(
                        function () {},
                        function () {}
                        );
                }
                catch(err) {
                }
            }
        };
    });