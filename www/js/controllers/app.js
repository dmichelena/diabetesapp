app.controller('AppController', function($scope, $rootScope, $ionicSideMenuDelegate,$ionicLoading, $ionicPopup, $location, userService) {
	$scope.isM = true;
	$scope.name = 'Invitado';
	$scope.$watch(function() {
        return userService.getName();
    }, function(newValue, oldValue) {
        $scope.name = newValue;
    });
    $scope.$watch(function() {
        return userService.getGender();
    }, function(newValue, oldValue) {
        $scope.isM = newValue == 'm';
    });



	$scope.logout = function(){
		$ionicLoading.show({noBackdrop : false});
		userService.logout()
			.success(function(data, status, headers, config) {
				$ionicLoading.hide();
				userService.deleteUserData();
			})
			.error(function(data, status, headers, config) {
				$rootScope.handleErrorForm($ionicLoading, $ionicPopup, status, data);
			});
	};
	$scope.sendAlert = function(){
		var confirmPopup = $ionicPopup.confirm({
			title: 'Registrar alerta',
			template: '¿Está seguro de registrar la alerta?'
		});

		confirmPopup.then(function(res) {
			if(res) {
				$ionicLoading.show({noBackdrop : false});
				userService.sendAlert()
					.success(function(data, status, headers, config) {
						$ionicPopup.alert({
							title: 'Aviso',
							template: 'Mantenga la calma, en breve estaremos en contacto.'
						});
						$ionicLoading.hide();
					})
					.error(function(data, status, headers, config) {
						$rootScope.handleErrorForm($ionicLoading, $ionicPopup, status, data);
					});
			}
		});
	};

	$scope.toggleLeft = function() {
		$ionicSideMenuDelegate.toggleLeft();
	};

});