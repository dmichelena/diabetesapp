app.controller('RangeController', function($scope, $rootScope, $location, userService, $ionicLoading, $ionicModal, $ionicPopup) {
    $scope.ranges = [];
    userService.getRanges()
        .success(function(data, status, headers, config) {
            $scope.ranges = data;
        })
        .error(function(data, status, headers, config) {
            $rootScope.handleErrorForm($ionicLoading, $ionicPopup, status, data);
            $location.path('/app/index');
        });

    $scope.isPatient = false;
    $scope.$watch(function() {
        return userService.isPatient();
    }, function(newValue, oldValue) {
        $scope.isPatient = newValue;
    });

    $scope.rangeData = {};
	$scope.edit = false;

	/*START-MODAL*/
	$ionicModal.fromTemplateUrl('templates/partials/rangeForm.html', {
        scope: $scope
    }).then(function(modal) {
        $scope.modal = modal;
    });
    $scope.hideRangeForm = function() {
        $scope.modal.hide();
        $scope.rangeData = {};
        $scope.edit = false;
    };
    $scope.new = function(data){
        if(data){
            $scope.rangeData = data;
            $scope.edit = true;
        }
        $rootScope.setCallback($scope.hideRangeForm);
        $scope.modal.show();
    };

    $scope.deleteRange = function(data){
        $ionicLoading.show({noBackdrop : false});
        userService.deleteRange(data)
            .success(function(data, status, headers, config) {
                $ionicLoading.hide();
                $scope.ranges = data;
                $ionicPopup.alert({
                    title: 'Aviso',
                    template: 'Se eliminó correctamente el rango.'
                });
            })
             .error(function(data, status, headers, config) {
                $rootScope.handleErrorForm($ionicLoading, $ionicPopup, status, data);
            });
    };

    $scope.delete = function(data){
       var confirmPopup = $ionicPopup.confirm({
         title: 'Alerta',
         template: '¿Seguro de eliminar el rango seleccionado?'
       });

       confirmPopup.then(function(res) {
         if(res) {
           $scope.deleteRange(data);
         } 
       });
     };

	/*END-MODAL*/

	$scope.registerRange = function(){
         $ionicLoading.show({noBackdrop : false});
		 userService.registerRange($scope.rangeData)
            .success(function(data, status, headers, config) {
                $ionicLoading.hide();
                $scope.rangeData = {};
                $scope.edit = false;
                $scope.ranges = data;
				$ionicPopup.alert({
					title: 'Aviso',
					template: 'Se guardó correctamente el rango.'
				});
                $scope.hideRangeForm();
            })
             .error(function(data, status, headers, config) {
                $rootScope.handleErrorForm($ionicLoading, $ionicPopup, status, data);
            });
	};
});