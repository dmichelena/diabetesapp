controllers.service('typeAheadService', function(restService){
    return {
        getMedicaments: function (term){
            return restService.post('medicine/search', {term:term});
        },
     	getFood: function (term){
            return restService.post('food/search',{term:term});
        },   
    };
});