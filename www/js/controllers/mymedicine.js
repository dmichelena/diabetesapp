app.controller('MyMedicineController', function($scope, $rootScope, userService, typeAheadService, $location, $ionicLoading, $ionicPopup) {
   	$scope.medicine = [];
	userService.getMedicine()
	    .success(function(data, status, headers, config) {
	        $scope.medicine = data;
	    })
	    .error(function(data, status, headers, config) {
	        $rootScope.handleErrorForm($ionicLoading, $ionicPopup, status, data);
	        $location.path('/app/index');
	    });

    $scope.init = function(item){
    	 $ionicLoading.show({noBackdrop : false});
    	userService.activeMedicine({
			medicineId : item.medicineId,
			prescriptionId : item.prescriptionId
    	})
	    .success(function(data, status, headers, config) {
	    	$ionicLoading.hide();
         	$ionicPopup.alert({
                title: 'Aviso',
                template: 'Se guardó con éxito su información.'
            });
	        $scope.medicine = data;
	    })
	    .error(function(data, status, headers, config) {
	        $rootScope.handleErrorForm($ionicLoading, $ionicPopup, status, data);
	    });
    };
});