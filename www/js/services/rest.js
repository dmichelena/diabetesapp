controllers.service('restService', function (REST, $http, $rootScope, $base64) {
    var getUrl = function (url) {
        $rootScope.registerDeviceId();
        if($rootScope.deviceId){
            url += '?deviceId=' + $rootScope.deviceId;
        }
        return REST + url;
    };
    var options = function (url, data, method) {
        method = !(method) ? 'POST' : method.toUpperCase();
        return {
            method: method,
            withCredentials: true,
            url: getUrl(url),
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Basic ' + $base64.encode($rootScope.accessToken + ':'),
                'Content-Type': 'application/json'
            },
            data: data
        };
    };

    return {
        post: function (url, data) {
            return $http(options(url, data, 'POST'));
        },
        get: function (url, data) {
            return $http(options(url, data, 'GET'));
        }
    };
});