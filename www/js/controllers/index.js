app.controller('IndexController', function($scope, $rootScope, userService, $ionicLoading, $ionicModal, $ionicPopup) {
	$scope.glucoseData = {};

    $scope.errorReport = false;
    userService.getReport()
        .success(function(data, status, headers, config) {
            $scope.labelsC = data.labelsC;
            $scope.labelsIG = data.labelsIG;
            $scope.dataIG = [data.dataIG];
            $scope.dataC = data.dataC;
            $scope.errorReport = false;
        })
         .error(function(data, status, headers, config) {
           $scope.errorReport = true;
        });

	/*START-MODAL*/
	$ionicModal.fromTemplateUrl('templates/partials/glucoseForm.html', {
        scope: $scope
    }).then(function(modal) {
        $scope.modal = modal;
    });
    $scope.hideGlucoseForm = function() {
        $scope.modal.hide();
    };
	$scope.showGlocuseForm = function(){
        $rootScope.setCallback($scope.hideGlucoseForm);
		$scope.modal.show();
	};
	/*END-MODAL*/


	$scope.registerGlucose = function(){
		 userService.registerGlucoseIndex($scope.glucoseData)
                .success(function(data, status, headers, config) {
                    $ionicLoading.hide();
                    $scope.glucoseData = {};
                    userService.addGlucoseIndex(data.model);
					$ionicPopup.alert({
						title: 'Aviso',
						template: data.message
					});
                    if(!$scope.errorReport){
                        $scope.addIndex(data.model);
                    }
                    $scope.hideGlucoseForm();
                })
                 .error(function(data, status, headers, config) {
                    $rootScope.handleErrorForm($ionicLoading, $ionicPopup, status, data);
                });
	};


    $scope.addIndex = function (model) {
        $scope.labelsIG.push(model.created_at);
        $scope.dataIG[0].push(model.glucoseIndex);
    };

    $scope.labelsIG = [];
    $scope.seriesIG = ['Índice de glucosa'];
    $scope.dataIG = [[]];

    $scope.labelsC = [];
    $scope.seriesC = ['Carbohidratos','Grasas','Azúcares'];
    $scope.dataC = [];

});