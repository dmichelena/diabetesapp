app.controller('ConsuptionController', function($scope, $rootScope, userService, typeAheadService, $location, $ionicLoading, $ionicPopup) {
  
    $scope.itemSelected = undefined;
    $scope.items = [];
    $scope.itemList = [];
    $scope.search = '';
    $scope.messages = [];

    $scope.ranges = [];
    $scope.totalConsuption;

    userService.getConsuptionData()
        .success(function(data, status, headers, config) {
            $scope.ranges = data.ranges;
            $scope.totalConsuption = data.totalConsuption;
            $scope.checkRanges();
        })
        .error(function(data, status, headers, config) {
            $rootScope.handleErrorForm($ionicLoading, $ionicPopup, status, data);
            $location.path('/app/index');
        });

    $scope.$watch('search', function(newValue, oldValue) {
        if($scope.search == ''){
            return $scope.cancel();
        }
        typeAheadService.getFood($scope.search).then(function (resp){
            $scope.items = $scope.filterSelected(resp.data);
        });
    });
    $scope.cancel = function () {
        $scope.items = [];
        $scope.search = '';
    };
    $scope.onItemSelected = function() {
        $scope.itemSelected.quantity = 1
        $scope.itemList.push($scope.itemSelected);
        $scope.checkRanges();
        $scope.cancel();
    };
    $scope.checkItemExist = function(item) {
        var count =  $scope.itemList.length;
        for(var i = 0; i < count; i++){
            if($scope.itemList[i].id == item.id){
                return true;
            }
        }
        return false;
    };
    $scope.filterSelected = function(items) {
        var count =  items.length;
        var listResponse = [];
        for(var i = 0; i < count; i++){
            if(!$scope.checkItemExist(items[i])){
                listResponse.push(items[i]);
            }
        }
        return listResponse;
    };
    $scope.delete = function(index){
        $scope.itemList.splice(index, 1);
        $scope.checkRanges();
    };
    $scope.save = function(){
        $ionicLoading.show({noBackdrop : false});
        userService.saveFood($scope.itemList) 
            .success(function(data, status, headers, config) {
                message = '<ul><li>Se guardoó con exito</li>';
                 for(var i = 0; i < $scope.messages.length; i++) {
                    message += '<li>' + $scope.messages[i] + '</li>';
                }
                message += '<ul>';
                $ionicLoading.hide();
                 $ionicPopup.alert({
                    title: 'Aviso',
                    template: message
                });
                $scope.cancel();
                $location.path('/app/index');
            })
             .error(function(data, status, headers, config) {
                $rootScope.handleErrorForm($ionicLoading, $ionicPopup, status, data);
            });
    };
    $scope.cancelFood = function(){
        $scope.itemList = [];
        $scope.cancel();
        $location.path('/app/index');
    };

    $scope.__addOne = function(item){
        if(item.quantity){
            return item.quantity += 1;
        }

        return item.quantity = 1;
    };
    $scope.addOne = function(item){
        $scope.__addOne(item);
        $scope.checkRanges();
    };
    $scope.removeOne = function(item){
        $scope.__removeOne(item);
        $scope.checkRanges();
    };
    $scope.__removeOne = function(item){
        if(item.quantity && item.quantity > 2){
            return item.quantity -= 1;
        }

        return item.quantity = 1;
    };

    $scope.checkRanges = function() {
        var rangeTmp;
        var total;
        var message = '';
        var messages = [];
        if($scope.hasRange('Azucar')){
            rangeTmp = $scope.getRange('Azucar');
            total = $scope.getTotal('sugar');
            if(rangeTmp.max < total){
                message = 'Te exediste en azúcar.';
            } else if(rangeTmp.min > total){
                message = 'Te falta consumir azúcares.';
            } else {
                message = 'Estas dentro del rango de azúcar.';
            }
            messages.push(message);
        }

        if($scope.hasRange('Carbohidratos')){
            rangeTmp = $scope.getRange('Carbohidratos');
            total = $scope.getTotal('carbohydrates');
            if(rangeTmp.max < total){
                message = 'Te exediste en carbohidratos.';
            } else if(rangeTmp.min > total){
                message = 'Te falta consumir carbohidratos.';
            } else {
                message = 'Estas dentro del rango de carbohidratos.';
            }
            messages.push(message);
        }

        if($scope.hasRange('Grasas')){
            rangeTmp = $scope.getRange('Grasas');
            total = $scope.getTotal('fat');
            if(rangeTmp.max < total){
                message = 'Te exediste en grasas.';
            } else if(rangeTmp.min > total){
                message = 'Te falta consumir grasas.';
            } else {
                message = 'Estas dentro del rango de grasas.';
            }
            messages.push(message);
        }

        $scope.messages = messages;
       
    };

    $scope.hasRange = function(range) {
        for(var i = 0; i < $scope.ranges.length; i++) {
            if($scope.ranges[i].type == range){
                return true;
            }
        }
        return false;
       
    };
    $scope.getRange = function(range) {
        for(var i = 0; i < $scope.ranges.length; i++) {
            if($scope.ranges[i].type == range){
                return $scope.ranges[i];
            }
        }
    };

     $scope.getTotal = function(range) {
        var total = $scope.totalConsuption[range];
        for(var i = 0; i < $scope.itemList.length; i++) {
            var food = $scope.itemList[i];
            total += food.quantity * food[range];
        }
        return total;
    };
});