controllers.service('publicService',function($http, REST_PUBLIC){
    var getUrl = function (url) {
        return REST_PUBLIC + url;
    };
    var options = function (url,data, method) {
        method = !(method) ? 'POST' : method.toUpperCase();
        return {
            method: method,
            withCredentials : true,
            url: getUrl(url),
            headers: {
                'Accept': 'application/json',
                // 'Authorization': 'Negotiate',
                'Content-Type': 'application/json'
            },
            data: data
        };
    };
    return {
        login: function (data){
            return $http(options('user/login',data));
        },
        register: function (data){
            return $http(options('user/register',data));
        }
    };
});