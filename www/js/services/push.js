function register(e) {
	registerPush(e);
}

controllers.service('pushService', function(SENDER_ID, $rootScope, $ionicPopup){
	window.registerPush = function (e){
		switch (e.event) {
			case 'registered':
				if (e.regid.length > 0) {
					$rootScope.deviceId = e.regid;
				}
				break;

			case 'message':
				if(e.payload && e.payload.title && e.payload.message){
					$ionicPopup.alert({
		                title: e.payload.title,
		                template: e.payload.message
		            });
				}
				break;

			case 'error':
				// handle error
				break;

			default:
				// handle default
				break;
		}
	};
    return {
        register: function (success, error){
            var pushNotification = window.plugins.pushNotification;
            pushNotification.register(success, error, {
                senderID: SENDER_ID,
                ecb : "register"
            });
        }
    };
});