app.directive('typeahead', function($timeout) {
		return {
			restrict: 'E',
			scope: {
				items: '=',
				title: '@',
				cancelButton: '@',
				model: '=',
				itemSelected: '=',
				onSelect: '&',
				onCancel: '&'
			},
			link: function(scope, elem, attrs) {
				scope.handleCancel = function() {
					scope.onCancel();
				};
				scope.handleSelection = function(item) {
					scope.itemSelected = item;
					$timeout(function() {
						scope.onSelect();
					}, 100);
				};
			},
			templateUrl: 'templates/partials/typeahead.html'
		};
	});