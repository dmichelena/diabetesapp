app.controller('LoginController', function($scope, $rootScope, $location, publicService, userService, $ionicLoading, $ionicPopup) {
    $scope.loginData = {};
    $scope.formIsValid = function() {
        return true;
    };
    $scope.doLogin = function() {
        if(!$scope.formIsValid()){
            return;
        }
        $ionicLoading.show({noBackdrop : false});
        publicService.login($scope.loginData)
            .success(function(data, status, headers, config) {
                $ionicLoading.hide();
                $scope.loginData = {};
                userService.setUser(data);
                $rootScope.startRefreshUser(true);
                $location.path('/app/index');
            })
            .error(function(data, status, headers, config) {
                $rootScope.handleErrorForm($ionicLoading, $ionicPopup, status, data);
            });
    };

});